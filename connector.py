import os, csv, pymysql, json, xlsxwriter, logging
from datetime import datetime, date

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)

class SQL:

    def __init__(self, host, username, password, database):
        self.host = host
        self.username = username
        self.password = password
        self.database = database
        self.connection = None

    def sql_query(self):
        purchase = """
        SELECT product.name,
            product.sku,
            contact.name,
            transaction.ref_no,
            transaction.transaction_date,
            purchase.quantity,
            purchase.purchase_price,
            payments.method
            FROM transactions transaction
            INNER JOIN contacts contact ON contact.id = transaction.contact_id
            INNER JOIN purchase_lines purchase ON transaction.id = purchase.transaction_id
            INNER JOIN products product ON product.id = purchase.product_id
            INNER JOIN transaction_payments payments ON transaction.id = payments.transaction_id
            WHERE transaction.type = 'purchase'
            ORDER BY `transaction`.`transaction_date` ASC
        """
        sell = """
            SELECT product.name,
                product.sku,
                CONCAT(contact.name, ' - ', contact.contact_id) ,
                transaction.invoice_no,
                transaction.transaction_date,
                sell.quantity,
                sell.unit_price,
                payments.method
                FROM transactions transaction
                INNER JOIN contacts contact ON contact.id = transaction.contact_id
                INNER JOIN transaction_sell_lines sell ON transaction.id = sell.transaction_id
                INNER JOIN transaction_payments payments ON transaction.id = payments.transaction_id
                INNER JOIN products product ON product.id = sell.product_id
                WHERE transaction.type = 'sell'
                ORDER BY transaction.transaction_date
                ORDER BY `transaction`.`transaction_date` ASC
        """
        return query

    def open_connection(self):
        try:
            if self.connection is None:
                self.connection = pymysql.connect(
                    self.host,
                    self.username,
                    self.password,
                    self.database,
                    connect_timeout=5
                )
        except pymysql.MySQLError as e:
            logging.error("MySQL Error {0}".format(e))
            sys.exit()
        finally:
            logging.info('Connection opened successfully.')

    def get_sql_result(self):
        self.open_connection()
        datas = []
        try:
            with self.connection.cursor() as cursor:
                query = self.sql_query()
                cursor.execute(query)
                rows = cursor.fetchall()

                for row in rows:
                    data = [ record for record in row]
                    date = row[4].strftime('%Y-%m-%d')
                    quantity = int(row[5])
                    price = float(row[6])
                    data[4] = date
                    data[5] = quantity
                    data[6] = price
                    datas.append(data)
            json_formatted_str = json.dumps(datas, indent=2)
            print(json_formatted_str)
        except pymysql.MySQLError as e:
            logging.error("MySQL Error {0}".format(e))
            sys.exit()
        finally:
            if self.connection:
                self.connection.close()
                logging.info("SQL Query successfully.")
                return datas


class Generate:

    transaction_type = 'purchase'
    today = date.today()

    def __init__(self):
        self.transactions = {}
        self.account_debit = "Pembelian Sparepart & Oli"
        self.account_ppn = "Hutang PPN"
        self.ppn_partner = ["PT. BERLIAN JAYA PERKASA", "PT.YAMAHA INDONESIA"]
        self.workbook = None

        if self.transaction_type == 'purchase':
            # headers=["Produk","SKU","Pemasok","Nomor Referensi", "Tanggal","Kwantitas","Subtotal"]
            self.filename="Pembelian Product"+ str(self.today) +".xlsx"
            self.worksheet = "Pembelian"
        else:
            # self.headers=["Produk","SKU","Nama Pelanggan","No. Faktur", "Tanggal","Kwantitas","Total"]
            self.filename="Penjualan Product"+ str(self.today) +".xlsx"
            self.worksheet = "Penjualan"

        self.files = os.path.join('/home/nurhasan/Downloads/' + self.filename)

    def get_credit(self, payment_method):
        if payment_method == 'cash':
            account_credit = "Kas Bengkel"
        else:
            account_credit = "BCA - A/C.3073008286"
        return account_credit

    def get_journal(self, payment_method):
        if payment_method == 'cash':
            journal = "Kas Bengkel"
        else:
            journal = "BCA - A/C.3073008286"
        return journal

    def add_data(self, row):
        data = {}
        reference = row['Reference']
        data['name'] = row['Produk']
        data['debit'] = float(row["Subtotal"])
        data['credit'] = 0.0
        data['account']= self.account_debit
        data['partner'] = row['Partner']
        self.transactions[reference]['data'].append(data)

    def create_dict(self, datas):
        for data in datas:
            rows = {}
            rows['Produk'] = data[0]
            rows['Partner'] = data[2]
            rows['Subtotal'] = data[6]
            reference = data[3]
            date = data[4]
            payment_method = data[7]

            if reference not in self.transactions:
                self.transactions[reference] = {}
                self.transactions[reference]['date'] = date
                # if rows['Partner'] in self.ppn_partner:
                #     coa = 'bank'
                self.transactions[reference]['journal'] = self.get_journal(payment_method)
                self.transactions[reference]['data'] = []
                self.transactions[reference]['method'] = payment_method
            rows['Reference'] = reference
            self.add_data(rows)
        # Test
        # json_formatted_str = json.dumps(transactions, indent=2)
        # print(json_formatted_str)
    # Add PPN Line
    def create_ppn_line(self, transactions):
        for transaction in transactions:
            partner = None
            total_credit = 0.0
            for data in self.transactions[transaction]['data']:
                # print(data)
                total_credit+=float(data['debit'])
                if not partner:
                    partner = data['partner']
            
            if data['partner'] in self.ppn_partner:
                total_ppn = (total_credit  / 100.0) * 10.0
                data = {}
                data['name'] = self.account_ppn
                data['debit'] = total_ppn
                data['credit'] = 0.0
                data['account']= self.account_ppn
                data['partner'] = partner
                self.transactions[transaction]['data'].append(data)

    def create_debit_line(self, transactions):
        # Add Debit Line
        for transaction in transactions:
            partner = None
            payment_method = self.transactions[transaction]['method']
            total_credit = 0
            coa = 'kas'
            for data in self.transactions[transaction]['data']:
                total_credit+=float(data['debit'])
                if not partner:
                    partner = data['partner']
                # if data['partner'] in self.ppn_partner:
                #     coa = 'bank'
            
            data = {}
            data['name'] = self.get_credit(payment_method)
            data['debit'] = 0.0
            data['credit'] = total_credit
            data['account']= self.get_credit(payment_method)
            data['partner'] = partner
            self.transactions[transaction]['data'].append(data)

    def create_excel(self, datas):
        self.create_dict(datas)
        self.create_ppn_line(self.transactions)
        self.create_debit_line(self.transactions)
        # json_formatted_str = json.dumps(transactions, indent=2)
        # print(json_formatted_str)
        try:
            self.workbook = xlsxwriter.Workbook(self.files)
            worksheet = self.workbook.add_worksheet(self.worksheet)

            data_format = self.workbook.add_format({'font_size': 11})

            worksheet.write('A1', 'Reference')
            worksheet.write('B1', 'Date')
            worksheet.write('C1', 'Journal')
            worksheet.write('D1', 'Journal Items/Account')
            worksheet.write('E1', 'Journal Items/Partner')
            worksheet.write('F1', 'Journal Items/Name')
            worksheet.write('G1', 'Journal Items/Debit')
            worksheet.write('H1', 'Journal Items/Credit')

            num_row = 1
            for transaction in self.transactions:
                records = self.transactions[transaction]
                datas = records['data'][0]
                # print(datas)
                worksheet.write(num_row, 0, transaction, data_format)
                worksheet.write(num_row, 1, records['date'], data_format)
                worksheet.write(num_row, 2, records['journal'], data_format)
                worksheet.write(num_row, 3, datas['account'], data_format)
                worksheet.write(num_row, 4, datas['partner'], data_format)
                worksheet.write(num_row, 5, datas['name'], data_format)
                worksheet.write(num_row, 6, datas['debit'], data_format)
                worksheet.write(num_row, 7, datas['credit'], data_format)
                
                if len(records['data']) > 1:
                    for data in records['data'][1:]:
                        num_row+=1
                        worksheet.write(num_row, 3, data['account'], data_format)
                        worksheet.write(num_row, 4, data['partner'], data_format)
                        worksheet.write(num_row, 5, data['name'], data_format)
                        worksheet.write(num_row, 6, data['debit'], data_format)
                        worksheet.write(num_row, 7, data['credit'], data_format)
                num_row+=1
        except:
            logging.error("Something wrong in xlsxwriter")
            sys.exit()
        finally:
            if self.workbook:
                self.workbook.close()
                logging.info("Workbook add successfully.")



SQL = SQL('192.168.1.106', 'root', '', 'aryaniagakt')
records = SQL.get_sql_result()

generate = Generate()
generate.create_excel(records)
