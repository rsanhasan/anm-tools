import csv, json, xlsxwriter

transactions = {}
account_credit = "Penjualan oli & SP"
# account_credit = "Kas Bengkel"
account_ppn = "Hutang PPN"
# ppn_partner = ["PT. BERLIAN JAYA PERKASA", "PT.YAMAHA INDONESIA"]

def get_debit(coa):
    if coa == 'kas':
        account_debit = "Kas Bengkel"
    else:
        account_debit = "BCA - A/C.3073008286"
    return account_debit

def get_journal(coa):
    if coa == 'kas':
        journal = "Kas Bengkel"
    else:
        journal = "BCA - A/C.3073008286"
    return journal

def add_data(row):
    data = {}
    data['name'] = row['Produk']
    data['credit'] = float(row["Total"])
    data['debit'] = 0.0
    data['account']= account_debit
    data['partner'] = row['Nama Pelanggan']
    transactions[reference]['data'].append(data)

state = None
with open('Laporan Pembelian Penjualan - September.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        reference=row['No. Faktur'] 
        date=row['Tanggal']
        if reference not in transactions:
            transactions[reference] = {}
            transactions[reference]['date'] = date
            coa = 'kas'
            if row['Nama Pelanggan'] in ppn_partner:
                coa = 'bank'
            transactions[reference]['journal'] = get_journal(coa)
            transactions[reference]['data'] = []
        add_data(row)

# Add PPN Line
for transaction in transactions:
    partner = None
    total_credit = 0.0
    for data in transactions[transaction]['data']:
        print(data)
        total_credit+=float(data['debit'])
        if not partner:
            partner = data['partner']
    if data['partner'] in ppn_partner:
        total_ppn = (total_credit  / 100.0) * 10.0
        data = {}
        data['name'] = account_ppn
        data['debit'] = total_ppn
        data['credit'] = 0.0
        data['account']= account_ppn
        data['partner'] = partner
        transactions[transaction]['data'].append(data)

# Add Credit Line
for transaction in transactions:
    partner = None
    total_debit = 0
    coa = 'kas'
    for data in transactions[transaction]['data']:
        total_debit+=float(data['credit'])
        if not partner:
            partner = data['partner']
        if data['partner'] in ppn_partner:
            coa = 'bank'

    data = {}
    data['name'] = get_debit(coa)
    data['credit'] = 0.0
    data['debit'] = total_debit
    data['account']= get_debit(coa)
    data['partner'] = partner
    transactions[transaction]['data'].append(data)

json_formatted_str = json.dumps(transactions, indent=2)
print(json_formatted_str)
workbook = xlsxwriter.Workbook('hello2.xlsx')
worksheet = workbook.add_worksheet("Pembelian")

data_format = workbook.add_format({'font_size': 11})

worksheet.write('A1', 'Reference')
worksheet.write('B1', 'Date')
worksheet.write('C1', 'Journal')
worksheet.write('D1', 'Journal Items/Account')
worksheet.write('E1', 'Journal Items/Partner')
worksheet.write('F1', 'Journal Items/Name')
worksheet.write('G1', 'Journal Items/Debit')
worksheet.write('H1', 'Journal Items/Credit')

num_row = 1
for transaction in transactions:
    records = transactions[transaction]
    datas = records['data'][0]
    # print(datas)
    worksheet.write(num_row, 0, transaction, data_format)
    worksheet.write(num_row, 1, records['date'], data_format)
    worksheet.write(num_row, 2, records['journal'], data_format)
    worksheet.write(num_row, 3, datas['account'], data_format)
    worksheet.write(num_row, 4, datas['partner'], data_format)
    worksheet.write(num_row, 5, datas['name'], data_format)
    worksheet.write(num_row, 6, datas['debit'], data_format)
    worksheet.write(num_row, 7, datas['credit'], data_format)
    
    if len(records['data']) > 1:
        for data in records['data'][1:]:
            num_row+=1
            worksheet.write(num_row, 3, data['account'], data_format)
            worksheet.write(num_row, 4, data['partner'], data_format)
            worksheet.write(num_row, 5, data['name'], data_format)
            worksheet.write(num_row, 6, data['debit'], data_format)
            worksheet.write(num_row, 7, data['credit'], data_format)
    num_row+=1
workbook.close()

# print(len(transactions))