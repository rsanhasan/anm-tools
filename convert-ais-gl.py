import csv, json, xlsxwriter

transactions = {}
journal = [{
        'BC3':'BCA - A/C.3073031008',
        'BC2':'BCA - A/C.3073008286',
        'BD1':'Danamon - A/C.000101427201',
        'BD2':'Danamon - A/C.003545823050',
        'K1':'Kas Showroom',
        'KB':'Kas Bengkel',
        'JM':'Jurnal Memorial',
        'PHJ':'Penjualan Motor'},
    {
        'BCA1':'BCA - A/C.3073004388',
        'BD1':'Danamon A/C. 4239240',
        'BD4':'Danamon - A/C.3545823548',
        'KB':'Kas Bengkel',
        'MM':'Jurnal Memorial',
        'PHJ':'Penjualan Motor',
        'K':'Kas Showroom [K]',
        'KS':'Kas Showroom [KS]',
    }]
filename= 'BiayaLembur-UangMakan-September.xlsx'

source_file = '/home/nurhasan/Kas-Showroom.csv'

def add_data(row):
    data = {}
    data['name'] = row['Description']
    data['credit'] = float(row['Credit'])
    data['debit'] = float(row['Debit'])
    data['account']= row['Nama Akun']
    transactions[reference]['data'].append(data)

with open(source_file, mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        reference=row['No. Jurnal']
        code=reference.split("/")[0]
        date=row['Date'].split(" ")[0]
        date_format="{0}/{1}/20{2}".format(date.split("/")[1],date.split("/")[0],date.split("/")[2])
        if reference not in transactions:
            transactions[reference] = {}
            transactions[reference]['date'] = date
            transactions[reference]['journal'] = journal[1][code]
            transactions[reference]['data'] = []
        add_data(row)

json_formatted_str = json.dumps(transactions, indent=2)
print(json_formatted_str)

workbook = xlsxwriter.Workbook(filename)
worksheet = workbook.add_worksheet("Pembelian")

data_format = workbook.add_format({'font_size': 11})

worksheet.write('A1', 'Reference')
worksheet.write('B1', 'Date')
worksheet.write('C1', 'Journal')
worksheet.write('D1', 'Journal Items/Account')
worksheet.write('E1', 'Journal Items/Name')
worksheet.write('F1', 'Journal Items/Debit')
worksheet.write('G1', 'Journal Items/Credit')

num_row = 1
for transaction in transactions:
    records = transactions[transaction]
    datas = records['data'][0]
    # print(datas)
    worksheet.write(num_row, 0, transaction, data_format)
    worksheet.write(num_row, 1, records['date'], data_format)
    worksheet.write(num_row, 2, records['journal'], data_format)
    worksheet.write(num_row, 3, datas['account'], data_format)
    worksheet.write(num_row, 4, datas['name'], data_format)
    worksheet.write(num_row, 5, datas['debit'], data_format)
    worksheet.write(num_row, 6, datas['credit'], data_format)
    
    if len(records['data']) > 1:
        for data in records['data'][1:]:
            num_row+=1
            worksheet.write(num_row, 3, data['account'], data_format)
            worksheet.write(num_row, 4, data['name'], data_format)
            worksheet.write(num_row, 5, data['debit'], data_format)
            worksheet.write(num_row, 6, data['credit'], data_format)
    num_row+=1
workbook.close()

# # print(len(transactions))