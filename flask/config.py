import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    TESTING = False
    CSRF_ENABLED = True
    DOWNLOAD_FOLDER = os.path.join(basedir, 'downloads/')
    LOGFILE = os.environ.get('LOGFILE')

class ProductionConfig(Config):
    DEBUG = False
    SECRET_KEY = "9d582ee9a55876148309b92b"

class DevelopmentConfig(Config):
    DEBUG=True
    DEVELOPMENT = True
    SECRET_KEY = "3e539c538c1eec98d6b4c0ca"
    