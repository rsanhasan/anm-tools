import os, csv, json
from app import app
from io import StringIO
from flask import Flask, request, redirect, url_for, render_template, send_from_directory
from werkzeug.utils import secure_filename
from app.routes import tools
from app.routes.forms import ReportsForm

@app.route('/', methods=['GET'])
def get_index():
    form = ReportsForm()
    return render_template('index.html', form=form)

@app.route('/', methods=['POST'])
def post_index():
    form = ReportsForm()
    params = {}
    params['type_transaction'] = form.type_transaction.data
    params['start_date'] = form.start_date.data
    params['end_date'] = form.end_date.data
    transaction_type = params['type_transaction']
    # SQL = tools.SQL(host='ip', port=3306, username='root', password='', database='databasename')
    date_range = {'start_date': params['start_date'], 'end_date': params['end_date']}
    records = SQL.get_sql_result(date_range, transaction_type)

    generate = tools.Generate(params)
    generate.create_excel(records)
    files = generate.filename
    return redirect(url_for('downloads', files=files))

@app.route('/downloads/<files>')
def downloads(files):
    return send_from_directory(app.config['DOWNLOAD_FOLDER'], files, as_attachment=True)
