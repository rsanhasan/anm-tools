import sys, os, csv, pymysql, json, xlsxwriter
from datetime import datetime, date
from app import app

class SQL:

    def __init__(self, host, port, username, password, database):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.database = database
        self.connection = None
        self.type = None

    def sql_query(self, date_range):
        start_date = date_range['start_date'].replace('/', '-')
        end_date = date_range['end_date'].replace('/', '-')
        purchase = """
        SELECT product.name,
            product.sku,
            contact.name,
            transaction.ref_no,
            transaction.transaction_date,
            purchase.quantity,
            purchase.purchase_price,
            payments.method,
            unit.actual_name
            FROM transactions transaction
            INNER JOIN contacts contact ON contact.id = transaction.contact_id
            INNER JOIN purchase_lines purchase ON transaction.id = purchase.transaction_id
            INNER JOIN products product ON product.id = purchase.product_id
            INNER JOIN transaction_payments payments ON transaction.id = payments.transaction_id
            INNER JOIN units unit ON unit.id = product.unit_id
            WHERE transaction.type = 'purchase' AND ( transaction.transaction_date BETWEEN '{0} 00:00:00' AND '{1} 23:59:59' )
            ORDER BY `transaction`.`transaction_date` ASC
        """.format(start_date, end_date)

        sell = """
            SELECT product.name,
                product.sku,
                CONCAT(contact.name, ' - ', contact.contact_id) ,
                transaction.invoice_no,
                transaction.transaction_date,
                sell.quantity,
                sell.unit_price,
                payments.method,
                unit.actual_name
                FROM transactions transaction
                INNER JOIN contacts contact ON contact.id = transaction.contact_id
                INNER JOIN transaction_sell_lines sell ON transaction.id = sell.transaction_id
                INNER JOIN transaction_payments payments ON transaction.id = payments.transaction_id
                INNER JOIN products product ON product.id = sell.product_id
                INNER JOIN units unit ON unit.id = product.unit_id
                WHERE transaction.type = 'sell' AND ( transaction.transaction_date BETWEEN '{0} 00:00:00' AND '{1} 23:59:59' ) AND (NOT transaction.final_total = 0.0)
                ORDER BY `transaction`.`transaction_date` ASC
        """.format(start_date, end_date)

        if self.type == 'sell':
            return sell
        else:
            return purchase

    def open_connection(self):
        try:
            if self.connection is None:
                self.connection = pymysql.connect(
                    host=self.host,
                    port=self.port,
                    user=self.username,
                    passwd=self.password,
                    db=self.database,
                    connect_timeout=5
                )
        except pymysql.MySQLError as e:
            app.logger.error("MySQL Error {0}".format(e))
            sys.exit()
        finally:
            if self.connection:
                app.logger.info('Connection opened successfully.')

    def get_sql_result(self, date_range, trasaction_type):
        self.open_connection()
        self.type = trasaction_type
        datas = []
        try:
            with self.connection.cursor() as cursor:
                query = self.sql_query(date_range=date_range)
                cursor.execute(query)
                rows = cursor.fetchall()

                for row in rows:
                    data = [ record for record in row]
                    date = row[4].strftime('%Y-%m-%d')
                    quantity = int(row[5])
                    price = float(row[6])
                    data[4] = date
                    data[5] = quantity
                    data[6] = price
                    datas.append(data)
        except pymysql.MySQLError as e:
            app.logger.error("MySQL Error {0}".format(e))
            sys.exit()
        finally:
            if self.connection:
                self.connection.close()
                app.logger.info("SQL Query successfully.")
                return datas


class Generate:

    today = date.today()

    def __init__(self, params):
        self.transactions = {}
        self.partner = ["PT. BERLIAN JAYA PERKASA", "PT.YAMAHA INDONESIA", "NIAGA CENGKARENG"]
        self.workbook = None
        self.params = params
        self.transaction_type = self.params['type_transaction']
        self.journal = "Kas Bengkel"
        self.labels = {}
        self.accounts = {}

        if self.transaction_type == 'purchase':
            self.filename="Pembelian Product - "+ str(self.today) +".xlsx"
            self.worksheet = "Pembelian"
        else:
            self.filename="Penjualan Product - "+ str(self.today) +".xlsx"
            self.worksheet = "Penjualan"
        self.files = os.path.join(app.config['DOWNLOAD_FOLDER'], self.filename)
    
    def set_account_label(self, transaction_type):
        if transaction_type == 'sell':
            self.labels['Income Services'] = "Trm Pend Jasa Service"
            self.labels['Income Products'] = "Trm Pemb Sparepart & Oli"
            self.labels['PPN Products'] = "PPN Trm Pemb Sparepart & Oli"
            self.labels['PPN Services'] = "PPN Trm Pend Jasa Service"
            self.labels['Total Income'] = "Trm Pemb Sparepart & Oli"
            self.accounts['Income Services'] = "Pendapatan jasa service"
            self.accounts['Income Products'] = "Penjualan oli & SP"
            self.accounts['PPN Products'] = "Hutang PPN"
            self.accounts['PPN Services'] = "Hutang PPN"
            self.accounts['Total Income'] = "Kas Bengkel"

        # def purchase_label():
        #     self.labels['Expense Products'] = "Trm Pemb Sparepart & Oli"
        #     self.labels['Total Cost'] = "Trm Pemb Sparepart & Oli"

        # def purchase_account():
        #     self.accounts['Expense Products'] = ""
        #     self.accounts['Total Cost'] = ""
   

    def add_data(self, row):
        data = {}
        data['total'] = float(row["Subtotal"]) * float(row['Quantity'])
        reference = row['Reference']
        data['name'] = row['Produk']
        data['type'] = row['Unit']
        
        self.transactions[reference]['data'].append(data)

    def create_dict(self, datas):
        check_data= {}
        for data in datas:
            rows = {}
            rows['Produk'] = data[0]
            rows['Partner'] = data[2]
            rows['Subtotal'] = data[6]
            rows['Unit'] = data[8]
            rows['Quantity'] = data[5]
            reference = data[3]
            date = data[4]
            payment_method = data[7]

            # Add new key to dict if not availabel
            if reference not in self.transactions:
                check_data[reference] = []
                self.transactions[reference] = {}
                self.transactions[reference]['date'] = date
                self.transactions[reference]['data'] = []

            # Add new key to dict if check is false
            if data[1] not in check_data[reference]:
                check_data[reference].append(data[1])
                rows['Reference'] = reference
                self.add_data(rows)

    def grouping_type(self, transactions):
        # Create dict for total debit and credit
        def add_total(transaction, name, credit, debit):
            self.transactions[transaction][name] = {
                'debit': debit,
                'credit': credit
            }

        for transaction in transactions:
            datas=self.transactions[transaction]['data']
            total_services = 0.0
            total_items = 0.0
            for data in datas:
                # Check type of transactions
                if data['type'].lower() == 'jasa': 
                    total_services+=data['total']
                else:
                    total_items+=data['total']
            # Add total amounts
            add_total(transaction, 'Total Services', total_services, 0.0)
            add_total(transaction, 'Total Products', total_items, 0.0)
        # [UNIT TESTING OF GROUPING TYPE OF TRANSACTIONS]
        # json_formatted_str = json.dumps(merge_transactions, indent=2)
        # app.logger.info(json_formatted_str)

    def merge_data(self, transactions):
        merge_transactions = {}
        for transaction in transactions:
            date = self.transactions[transaction]['date']
            # Add new key to dict if not availabel
            if date not in merge_transactions:
                merge_transactions[date] = {}
                merge_transactions[date]['Income Services'] = {'debit': 0.0, 'credit': 0.0}
                merge_transactions[date]['Income Products'] = {'debit': 0.0, 'credit': 0.0}
                merge_transactions[date]['PPN Products'] = {'debit': 0.0, 'credit': 0.0}
                merge_transactions[date]['PPN Services'] = {'debit': 0.0, 'credit': 0.0}
                merge_transactions[date]['Total Income'] = {'debit': 0.0, 'credit': 0.0}

            income_services = self.transactions[transaction]['Total Services']['credit']
            income_products = self.transactions[transaction]['Total Products']['credit']
            ppn_services = income_services - (income_services / 1.1)
            ppn_products = income_products - (income_products / 1.1)

            merge_transactions[date]['Income Services']['credit'] += (income_services - ppn_services)
            merge_transactions[date]['Income Products']['credit'] += (income_products - ppn_products)
            merge_transactions[date]['PPN Services']['credit'] += ppn_services
            merge_transactions[date]['PPN Products']['credit'] += ppn_products
            merge_transactions[date]['Total Income']['debit'] += round(income_services + income_products)

        # Rounding decimal on values of amount credit
        for transaction in merge_transactions:
            income_services = round(merge_transactions[transaction]['Income Services']['credit'])
            income_products = round(merge_transactions[transaction]['Income Products']['credit'])
            ppn_services = round(merge_transactions[transaction]['PPN Services']['credit'])
            ppn_products = round(merge_transactions[transaction]['PPN Products']['credit'])

            merge_transactions[transaction]['Income Services']['credit'] = income_services
            merge_transactions[transaction]['Income Products']['credit'] = income_products
            merge_transactions[transaction]['PPN Services']['credit'] = ppn_services
            merge_transactions[transaction]['PPN Products']['credit'] = ppn_products
        # [UNIT TESTING OF MERGING AMOUNT]
        self.transactions = merge_transactions
        # json_formatted_str = json.dumps(self.transactions, indent=2)
        # app.logger.info(json_formatted_str)


    def create_excel(self, datas):
        self.create_dict(datas)
        # json_formatted_str = json.dumps(self.transactions, indent=2)
        # print(json_formatted_str)
        self.grouping_type(self.transactions)
        self.merge_data(self.transactions)
        self.set_account_label(self.transaction_type)
        try:
            self.workbook = xlsxwriter.Workbook(self.files)
            worksheet = self.workbook.add_worksheet(self.worksheet)

            data_format = self.workbook.add_format({'font_size': 11})

            worksheet.write('A1', 'Date')
            worksheet.write('B1', 'Journal')
            worksheet.write('C1', 'Journal Items/Account')
            worksheet.write('D1', 'Journal Items/Name')
            worksheet.write('E1', 'Journal Items/Debit')
            worksheet.write('F1', 'Journal Items/Credit')

            num_row = 1
            for transaction in self.transactions:
                desc_index = 0
                records = self.transactions[transaction]
                descriptions = [key for key in records.keys()]
                worksheet.write(num_row, 0, transaction, data_format)
                worksheet.write(num_row, 1, self.journal, data_format)
                worksheet.write(num_row, 2, self.accounts[descriptions[desc_index]], data_format)
                worksheet.write(num_row, 3, self.labels[descriptions[desc_index]], data_format)
                worksheet.write(num_row, 4, records[descriptions[desc_index]]['debit'], data_format)
                worksheet.write(num_row, 5, records[descriptions[desc_index]]['credit'], data_format)
                
                for desc in descriptions[1:]:
                    num_row+=1
                    worksheet.write(num_row, 2, self.accounts[desc], data_format)
                    worksheet.write(num_row, 3, self.labels[desc], data_format)
                    worksheet.write(num_row, 4, records[desc]['debit'], data_format)
                    worksheet.write(num_row, 5, records[desc]['credit'], data_format)
                num_row+=1
        except:
            app.logger.error("Something wrong in xlsxwriter")
            sys.exit()
        finally:
            if self.workbook:
                self.workbook.close()
                app.logger.info("Workbook add successfully.")

