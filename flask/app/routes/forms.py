from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import DataRequired, Length

class ReportsForm(FlaskForm):
    """Reports form."""
    type_transaction = SelectField('Type Transactions', choices=[('sell', 'Sell')]) #choices=[('purchase', 'Purchase'), ('sell', 'Sell')])
    start_date = StringField('Start Date',  [DataRequired()])
    end_date = StringField('End Date', [DataRequired()])
    bank_account = SelectField('Bank', choices=[('bca1', 'BCA - A/C.3073008286'), ('bca2', 'BCA - A/C.3073031008')])
    cash_account = SelectField('Cash', choices=[('kb', 'Kas Bengkel'), ('ks', 'Kas Showroom')])
    tax_account = SelectField('PPN', choices=[('ppn', 'Hutang PPN')])
    type_account = SelectField('Income/Expense', choices=[('purchase', 'Pembelian Sparepart & Oli'), ('sell', 'Penjualan oli & SP')])
    submit = SubmitField('Download')