from flask import Flask
from config import DevelopmentConfig
import logging

# Init App
app = Flask(__name__)
app.config.from_object(DevelopmentConfig)

# Log config
# Add `filename=logfile` on Config to make application logging in file
# logfile = app.config['LOGFILE']
logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.INFO) 

# Run Server
from app import routes